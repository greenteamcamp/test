var modal = document.getElementById('js-gallery__block-hidden');
var modalImg = document.getElementById("js-gallery__img--big");
var span = document.getElementById("js-gallery__button--close");

var images = document.getElementsByClassName('js-gallery__img--small');

for (var i = 0; i < images.length; i++) {
    images[i].onclick = function() {
        modal.style.display = "block";
        modalImg.src = this.src;
    }
}

span.onclick = function() {
    modal.style.display = "none";
}
