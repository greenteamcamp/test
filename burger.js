var mobileList = document.getElementsByClassName('js-nav__ul-menu')[0];

window.onclick = function(a) {
    if (a.target.matches('#js-nav__button-menu')) {
        mobileList.classList.toggle("js-show");
    } else if (a.target.matches('.js-nav__link-menu')) {
        mobileList.classList.add("js-show");
    } else {
        mobileList.classList.remove("js-show");
    }
}